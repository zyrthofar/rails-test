class UsersController < ApplicationController
    def index
        render json: User.all
    end

    def create
        User.create!(params.permit(:name))
        render status: 201
    end

    def index
        render json: User.find(params.require(:id))
    end
end
